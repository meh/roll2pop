// Copyright (C) 2021 Mathieu Duponchelle <mathieu@centricular.com>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use glib::subclass::prelude::*;
use gst::prelude::*;
use gst::subclass::prelude::*;
use gst::{
    element_error, gst_debug, gst_error, gst_info, gst_log, gst_trace, gst_warning, loggable_error,
};

use crate::ttutils::{Cea608Mode, Lines};

use once_cell::sync::Lazy;

use std::collections::VecDeque;
use std::sync::{Condvar, Mutex};

const DEFAULT_LATENCY_MS: u32 = 10000;
const DEFAULT_ROWS: u32 = 2;

const GRANULARITY_MS: u32 = 33;

#[derive(Debug)]
struct TimestampedLines {
    lines: Lines,
    pts: gst::ClockTime,
    duration: gst::ClockTime,
}

struct State {
    pending: VecDeque<TimestampedLines>,
    clock_id: Option<gst::SingleShotClockId>,
    last_pts: gst::ClockTime,
}

impl Default for State {
    fn default() -> Self {
        State {
            pending: VecDeque::new(),
            clock_id: None,
            last_pts: 0.into(),
        }
    }
}

struct Settings {
    latency_ms: u32,
    rows: u32,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            latency_ms: DEFAULT_LATENCY_MS,
            rows: DEFAULT_ROWS,
        }
    }
}

pub struct Roll2Pop {
    srcpad: gst::Pad,
    sinkpad: gst::Pad,

    settings: Mutex<Settings>,
    state: Mutex<State>,
    live_cond: Condvar,
}

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "roll2pop",
        gst::DebugColorFlags::empty(),
        Some("Roll-up to Pop-on"),
    )
});

impl State {
    fn accumulate(
        &mut self,
        pts: gst::ClockTime,
        duration: gst::ClockTime,
        mut in_lines: Lines,
        rows: u32,
    ) {
        let base_row = 15 - rows;

        for mut line in in_lines.lines.drain(..) {
            let mut lines = match self.pending.pop_front() {
                Some(lines) => {
                    if lines.lines.lines.len() == rows as usize {
                        self.pending.push_front(lines);
                        TimestampedLines {
                            lines: Lines {
                                lines: Vec::new(),
                                mode: Some(Cea608Mode::PopOn),
                                clear: None,
                            },
                            pts,
                            duration: 0.into(),
                        }
                    } else {
                        lines
                    }
                }
                None => TimestampedLines {
                    lines: Lines {
                        lines: Vec::new(),
                        mode: Some(Cea608Mode::PopOn),
                        clear: None,
                    },
                    pts,
                    duration: 0.into(),
                },
            };

            line.row = Some(base_row + lines.lines.lines.len() as u32);
            lines.lines.lines.push(line);
            lines.duration = pts + duration - lines.pts;
            self.pending.push_front(lines);
        }
    }
}

impl Roll2Pop {
    fn output(
        &self,
        element: &super::Roll2Pop,
        mut lines: TimestampedLines,
    ) -> Result<gst::FlowSuccess, gst::FlowError> {
        gst_debug!(CAT, obj: element, "outputting: {:?}", lines);

        let mut state = self.state.lock().unwrap();
        let last_pts = state.last_pts;
        if lines.pts < last_pts {
            gst_warning!(CAT, obj: element, "Shortening");
            lines.duration -= last_pts - lines.pts;
            lines.pts = last_pts;
        }
        state.last_pts = lines.pts + lines.duration;
        drop(state);

        if last_pts < lines.pts {
            gst_debug!(CAT, "Pushing gap {} -> {}", last_pts, lines.pts);
            self.srcpad
                .push_event(gst::event::Gap::builder(last_pts, lines.pts - last_pts).build());
        }

        let json = serde_json::to_string(&lines.lines).map_err(|err| {
            gst::element_error!(
                element,
                gst::ResourceError::Write,
                ["Failed to serialize as json {}", err]
            );

            gst::FlowError::Error
        })?;

        let mut buf = gst::Buffer::from_mut_slice(json.into_bytes());
        {
            let buf_mut = buf.get_mut().unwrap();
            buf_mut.set_pts(lines.pts);
            buf_mut.set_duration(lines.duration);

            gst_debug!(
                CAT,
                obj: element,
                "Pushing buffer {} -> {}",
                lines.pts,
                lines.pts + lines.duration
            );
        }

        self.srcpad.push(buf)
    }

    fn loop_fn(&self, element: &super::Roll2Pop) -> Result<(), gst::FlowError> {
        let mut state = self.state.lock().unwrap();
        let mut clock = element.get_clock();

        let latency = self.settings.lock().unwrap().latency_ms as u64 * gst::MSECOND;

        let granularity = GRANULARITY_MS as u64 * gst::MSECOND;

        // Wait till we get to PLAYING
        while clock.is_none() {
            state = self.live_cond.wait(state).unwrap();
            clock = element.get_clock();
        }

        let clock = clock.unwrap();

        let time = element.get_current_running_time() + element.get_base_time();
        let timeout = clock.new_single_shot_id(time + 33 * gst::MSECOND);

        state.clock_id = Some(timeout.clone());

        drop(state);

        let (clock_ret, _jitter) = timeout.wait();

        if clock_ret == Err(gst::ClockError::Unscheduled) {
            return Err(gst::FlowError::Flushing);
        }

        let now = element.get_current_running_time();

        let mut state = self.state.lock().unwrap();

        let mut out_lines = Vec::new();

        while let Some(lines) = state.pending.pop_back() {
            if lines.pts + granularity < now - latency {
                gst_trace!(CAT, "Pushing out {:?} at rtime {}", lines, now);
                out_lines.push(lines);
            } else {
                state.pending.push_back(lines);
                break;
            }
        }

        drop(state);

        for lines in out_lines.drain(..) {
            self.output(element, lines)?;
        }

        let mut state = self.state.lock().unwrap();
        let last_pts = state.last_pts;
        if last_pts + granularity < now - latency {
            state.last_pts = now - latency;
        }
        drop(state);

        if last_pts + granularity < now - latency {
            gst_debug!(CAT, "Pushing gap {} -> {}", last_pts, now - latency);
            self.srcpad
                .push_event(gst::event::Gap::builder(last_pts, now - latency - last_pts).build());
        }

        Ok(())
    }

    fn start_task(&self, element: &super::Roll2Pop) -> Result<(), gst::LoggableError> {
        let element_weak = element.downgrade();
        let pad_weak = self.srcpad.downgrade();

        let res = self.srcpad.start_task(move || {
            let element = match element_weak.upgrade() {
                Some(element) => element,
                None => {
                    if let Some(pad) = pad_weak.upgrade() {
                        let _ = pad.pause_task();
                    }
                    return;
                }
            };

            let roll2pop = Self::from_instance(&element);
            match roll2pop.loop_fn(&element) {
                Err(err) => {
                    if err != gst::FlowError::Flushing && err != gst::FlowError::Eos {
                        element_error!(
                            &element,
                            gst::StreamError::Failed,
                            ["Streaming failed: {}", err]
                        );
                    }
                    let _ = roll2pop.srcpad.pause_task();
                }
                Ok(_) => (),
            };
        });
        if res.is_err() {
            return Err(loggable_error!(CAT, "Failed to start pad task"));
        }
        Ok(())
    }

    fn sink_chain(
        &self,
        pad: &gst::Pad,
        _element: &super::Roll2Pop,
        buffer: gst::Buffer,
    ) -> Result<gst::FlowSuccess, gst::FlowError> {
        gst_trace!(CAT, obj: pad, "Handling buffer {:?}", buffer);

        let data = buffer.map_readable().map_err(|_| {
            gst_error!(CAT, obj: pad, "Can't map buffer readable");

            gst::FlowError::Error
        })?;

        let lines: Lines = serde_json::from_slice(&data).map_err(|err| {
            gst_error!(CAT, obj: pad, "Failed to parse input as json: {}", err);

            gst::FlowError::Error
        })?;

        let pts = buffer.get_pts();
        if pts.is_none() {
            gst_error!(CAT, obj: pad, "Require timestamped buffers");
            return Err(gst::FlowError::Error);
        }

        let duration = buffer.get_duration();
        if duration.is_none() {
            gst_error!(CAT, obj: pad, "Require buffers with duration");
            return Err(gst::FlowError::Error);
        }

        let mut state = self.state.lock().unwrap();
        let settings = self.settings.lock().unwrap();

        state.accumulate(pts, duration, lines, settings.rows);

        Ok(gst::FlowSuccess::Ok)
    }

    fn sink_event(&self, pad: &gst::Pad, element: &super::Roll2Pop, event: gst::Event) -> bool {
        use gst::EventView;

        gst_log!(CAT, obj: pad, "Handling event {:?}", event);
        match event.view() {
            EventView::Caps(..) => {
                // We send our own caps downstream
                let caps = gst::Caps::builder("application/x-json")
                    .field("format", &"cea608")
                    .build();
                self.srcpad.push_event(gst::event::Caps::new(&caps))
            }
            EventView::FlushStart(_) => {
                gst_info!(CAT, obj: element, "Received flush start, disconnecting");
                let mut ret = pad.event_default(Some(element), event);

                match self.srcpad.stop_task() {
                    Err(err) => {
                        gst_error!(CAT, obj: element, "Failed to stop srcpad task: {}", err);
                        ret = false;
                    }
                    Ok(_) => (),
                };

                ret
            }
            EventView::FlushStop(..) => {
                if pad.event_default(Some(element), event) {
                    match self.start_task(element) {
                        Err(err) => {
                            gst_error!(CAT, obj: element, "Failed to start srcpad task: {}", err);

                            false
                        }
                        Ok(_) => {
                            let mut state = self.state.lock().unwrap();
                            *state = State::default();
                            true
                        }
                    }
                } else {
                    false
                }
            }
            _ => pad.event_default(Some(element), event),
        }
    }

    fn src_activatemode(
        &self,
        _pad: &gst::Pad,
        element: &super::Roll2Pop,
        _mode: gst::PadMode,
        active: bool,
    ) -> Result<(), gst::LoggableError> {
        if active {
            self.start_task(element)?;
        } else {
            let _ = self.srcpad.stop_task();
        }

        Ok(())
    }

    fn src_query(
        &self,
        pad: &gst::Pad,
        element: &super::Roll2Pop,
        query: &mut gst::QueryRef,
    ) -> bool {
        use gst::QueryView;

        match query.view_mut() {
            QueryView::Latency(ref mut q) => {
                let mut peer_query = gst::query::Latency::new();

                let ret = self.sinkpad.peer_query(&mut peer_query);

                if ret {
                    let (_, min, _) = peer_query.get_result();
                    let our_latency: gst::ClockTime =
                        self.settings.lock().unwrap().latency_ms as u64 * gst::MSECOND;
                    q.set(true, our_latency + min, gst::CLOCK_TIME_NONE);
                }
                ret
            }
            _ => pad.query_default(Some(element), query),
        }
    }
}

#[glib::object_subclass]
impl ObjectSubclass for Roll2Pop {
    const NAME: &'static str = "Roll2Pop";
    type Type = super::Roll2Pop;
    type ParentType = gst::Element;
    type Instance = gst::subclass::ElementInstanceStruct<Self>;

    fn with_class(klass: &Self::Class) -> Self {
        let templ = klass.get_pad_template("sink").unwrap();
        let sinkpad = gst::Pad::builder_with_template(&templ, Some("sink"))
            .chain_function(|pad, parent, buffer| {
                Roll2Pop::catch_panic_pad_function(
                    parent,
                    || Err(gst::FlowError::Error),
                    |this, element| this.sink_chain(pad, element, buffer),
                )
            })
            .event_function(|pad, parent, event| {
                Roll2Pop::catch_panic_pad_function(
                    parent,
                    || false,
                    |this, element| this.sink_event(pad, element, event),
                )
            })
            .flags(gst::PadFlags::FIXED_CAPS)
            .build();

        let templ = klass.get_pad_template("src").unwrap();
        let srcpad = gst::Pad::builder_with_template(&templ, Some("src"))
            .flags(gst::PadFlags::FIXED_CAPS)
            .activatemode_function(|pad, parent, mode, active| {
                Roll2Pop::catch_panic_pad_function(
                    parent,
                    || Err(loggable_error!(CAT, "Panic activating src pad with mode")),
                    |roll2pop, element| roll2pop.src_activatemode(pad, element, mode, active),
                )
            })
            .query_function(|pad, parent, query| {
                Roll2Pop::catch_panic_pad_function(
                    parent,
                    || false,
                    |roll2pop, element| roll2pop.src_query(pad, element, query),
                )
            })
            .build();

        Self {
            srcpad,
            sinkpad,
            settings: Mutex::new(Settings::default()),
            state: Mutex::new(State::default()),
            live_cond: Condvar::new(),
        }
    }
}

impl ObjectImpl for Roll2Pop {
    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        obj.add_pad(&self.sinkpad).unwrap();
        obj.add_pad(&self.srcpad).unwrap();
    }

    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![
                glib::ParamSpec::uint(
                    "rows",
                    "Rows",
                    "Maximum number of rows to accumulate",
                    1,
                    4,
                    DEFAULT_ROWS,
                    glib::ParamFlags::READWRITE | gst::PARAM_FLAG_MUTABLE_PLAYING,
                ),
                glib::ParamSpec::uint(
                    "latency",
                    "Latency",
                    "Amount of milliseconds to accumulate roll-up rows for",
                    66u32,
                    std::u32::MAX,
                    DEFAULT_LATENCY_MS,
                    glib::ParamFlags::READWRITE | gst::PARAM_FLAG_MUTABLE_PLAYING,
                ),
            ]
        });

        PROPERTIES.as_ref()
    }

    fn set_property(
        &self,
        obj: &Self::Type,
        _id: usize,
        value: &glib::Value,
        pspec: &glib::ParamSpec,
    ) {
        match pspec.get_name() {
            "latency" => {
                let mut settings = self.settings.lock().unwrap();
                let old_latency_ms = settings.latency_ms;
                settings.latency_ms = value.get_some().expect("type checked upstream");
                if settings.latency_ms != old_latency_ms {
                    gst_debug!(CAT, obj: obj, "Latency changed: {}", settings.latency_ms);
                    drop(settings);
                    let _ = obj.post_message(gst::message::Latency::builder().src(obj).build());
                }
            }
            "rows" => {
                let mut settings = self.settings.lock().unwrap();
                settings.rows = value.get_some().expect("type checked upstream");
            }
            _ => unimplemented!(),
        }
    }

    fn get_property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.get_name() {
            "latency" => {
                let settings = self.settings.lock().unwrap();
                settings.latency_ms.to_value()
            }
            "rows" => {
                let settings = self.settings.lock().unwrap();
                settings.rows.to_value()
            }
            _ => unimplemented!(),
        }
    }
}

impl ElementImpl for Roll2Pop {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "Roll-up to Pop-on",
                "Generic",
                "Converts Roll-up to Pop-on",
                "Mathieu Duponchelle <mathieu@centricular.com>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<gst::PadTemplate>> = Lazy::new(|| {
            let caps = gst::Caps::builder("application/x-json")
                .field("format", &"cea608")
                .build();

            let src_pad_template = gst::PadTemplate::new(
                "src",
                gst::PadDirection::Src,
                gst::PadPresence::Always,
                &caps,
            )
            .unwrap();

            let sink_pad_template = gst::PadTemplate::new(
                "sink",
                gst::PadDirection::Sink,
                gst::PadPresence::Always,
                &caps,
            )
            .unwrap();

            vec![src_pad_template, sink_pad_template]
        });

        PAD_TEMPLATES.as_ref()
    }

    fn change_state(
        &self,
        element: &Self::Type,
        transition: gst::StateChange,
    ) -> Result<gst::StateChangeSuccess, gst::StateChangeError> {
        gst_trace!(CAT, obj: element, "Changing state {:?}", transition);

        match transition {
            gst::StateChange::ReadyToPaused => {
                let mut state = self.state.lock().unwrap();
                *state = State::default();
            }
            gst::StateChange::PausedToPlaying => {
                let _state = self.state.lock().unwrap();
                self.live_cond.notify_one();
            }
            _ => (),
        }

        let mut ret = self.parent_change_state(element, transition)?;

        match transition {
            gst::StateChange::ReadyToPaused => {
                ret = gst::StateChangeSuccess::NoPreroll;
            }
            gst::StateChange::PlayingToPaused => {
                let mut state = self.state.lock().unwrap();
                if let Some(clock_id) = state.clock_id.take() {
                    clock_id.unschedule();
                }
                ret = gst::StateChangeSuccess::NoPreroll;
            }
            gst::StateChange::PausedToReady => {
                let mut state = self.state.lock().unwrap();
                *state = State::default();
            }
            _ => (),
        }

        Ok(ret)
    }
}
