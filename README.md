# roll2pop

## Build

``` shell
cargo build
```

## Use

``` shell
gst-launch-1.0 -e mpegtsmux name=mux ! filesink location=pop.ts filesrc location=library.mp4 ! qtdemux name=d \
  d.video_0 ! queue ! h264parse ! avdec_h264 ! clocksync ! \
    ccextractor name=cce remove-caption-meta=true \
      cce.src ! queue max-size-time=15000000000 max-size-bytes=0 max-size-buffers=0 ! \
        cccombiner name=ccc latency=500000000 ! clocksync ! x264enc tune=zerolatency ! queue ! mux. \
      cce.caption ! queue ! ccconverter ! cea608tojson ! roll2pop latency=10000 rows=2 ! tttocea608 ! \
        closedcaption/x-cea-608, framerate=30000/1001 ! ccconverter ! closedcaption/x-cea-708, format=cc_data ! \
        queue max-size-time=15000000000 max-size-bytes=0 max-size-buffers=0 ! ccc.caption \
  d.audio_0 ! clocksync ! queue max-size-time=15000000000 max-size-bytes=0 max-size-buffers=0 ! mux.
```
